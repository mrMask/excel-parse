﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Excel
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Instruction : Window
    {
        public Instruction()
        {
            InitializeComponent();
            ins(1);
        }
        void ins(int number)
        {
            if(number == 2)
            {
                btn_left.Visibility = Visibility.Visible;
                btn_right.Visibility = Visibility.Hidden;
                source.Source = new BitmapImage(new Uri(@"../../Resources/инструкция2.png", UriKind.Relative));
                text.Text =
                    "В меню можно посмотреть некоторую информацию о программе и разработчике\n"
                    + "Инсрукция - вызов инструкции\n"
                    + "Описание - некоторые сведения о программе\n"
                    + "О разработчике - некоторые сведения о разработчике";
            }
            else
            {
                btn_left.Visibility = Visibility.Hidden;
                btn_right.Visibility = Visibility.Visible;
                source.Source = new BitmapImage(new Uri(@"../../Resources/инструкция1.png", UriKind.Relative));
                text.Text =
                    "По нажатию на кнопку \"Выберите файл базы данных\" происходит выбор и последующее считывание файла\n"
                    + "По нажатию на кнопку \"Выберите редактируемый файл\" происходит выбор и последующее редактирование\nфайла";
            }
        }

        private void btn_right_Click(object sender, RoutedEventArgs e)
        {
            ins(2);
        }

        private void btn_left_Click(object sender, RoutedEventArgs e)
        {
            ins(1);
        }

    }
}
