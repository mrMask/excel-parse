﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
namespace Excel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : System.Windows.Window
    {
        private HSSFWorkbook data_wb;
        private HSSFWorkbook redact_wb;
        private List<string[]> data = new List<string[]>();
        Process prc = null;

        public MainWindow()
        {
        }
        string choose()
        {
            Microsoft.Win32.OpenFileDialog openDialog = new Microsoft.Win32.OpenFileDialog();
            openDialog.Filter = "Файл Excel|*.XLSX;*.XLS;*.XLSM";
            var result = openDialog.ShowDialog();
            string fileName = System.IO.Path.GetFileName(openDialog.FileName);
            string path = openDialog.FileName;
            return path;
        }
        private void choose_data_Click(object sender, RoutedEventArgs e)
        {
            string path = choose();
            choose_data.IsEnabled = false;
            choose_data.Content = "Файл базы данных выбран".ToString();
            try
            {
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    data_wb = new HSSFWorkbook(file);
                }
            }
            catch (System.ArgumentException exp)
            {
                MessageBox.Show("Файл не выбран, попробуйте снова", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                choose_data_Click(sender, e);
            }
            catch (System.IO.IOException exp)
            {
                MessageBox.Show("Скорее всего вы выбрали не файл базы данных, \nубедитесь в правильности выбора и повторите попытку", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                choose_data_Click(sender, e);
            }
            ISheet sheet = data_wb.GetSheetAt(0);
            int i = 1;
            var cell = sheet.GetRow(i);
            while ((cell != null) && (cell.GetCell(0).StringCellValue != ""))
            {
                data.Add(new string[] { null, cell.GetCell(5).StringCellValue, cell.GetCell(31).StringCellValue, null });
                i++;
                cell = sheet.GetRow(i);
            }
            data_wb.Close();
        }
        private void choose_redact_Click(object sender, RoutedEventArgs e)
        {
            choose_redact.IsEnabled = false;
            choose_redact.Content = "Файл для редактирования выбран";
            string redact_path = choose();
            try
            {
                using (FileStream redact_file = new FileStream(redact_path, FileMode.Open, FileAccess.Read))
                {

                    redact_wb = new HSSFWorkbook(redact_file);
                }
            }
            catch (System.ArgumentException exp)
            {
                MessageBox.Show("Файл не выбран, попробуйте снова", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                choose_redact_Click(sender, e);
            }
            catch (System.IO.IOException exp)
            {
                MessageBox.Show("Файл занят, попробуйте закрыть файл и повторить попытку", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                choose_redact_Click(sender, e);
            }
            ISheet sheet = redact_wb.GetSheetAt(0);
            int i = 1;
            while ((sheet.GetRow(i) != null) && (sheet.GetRow(i).GetCell(0).StringCellValue != ""))
            {
                foreach (string[] item in data)
                {
                    if ((item[1] == sheet.GetRow(i).GetCell(5).StringCellValue) && (item[0] == null))
                    {
                        item[0] = i.ToString();
                        if (sheet.GetRow(i).GetCell(5).StringCellValue == item[1])
                            item[3] = "True";
                        else
                            item[3] = "False";
                        break;
                    }
                }
                i++;
            }
            using (FileStream redact_file = new FileStream(redact_path, FileMode.Open, FileAccess.Write))
            {
                foreach (string[] item in data)
                {
                    if (item[0] != null)
                    {
                        sheet.GetRow(System.Convert.ToInt32(item[0])).CreateCell(32);
                        sheet.GetRow(System.Convert.ToInt32(item[0])).CreateCell(33);
                        sheet.GetRow(System.Convert.ToInt32(item[0])).CreateCell(34);
                        sheet.GetRow(System.Convert.ToInt32(item[0])).GetCell(32).SetCellValue(item[1]);
                        sheet.GetRow(System.Convert.ToInt32(item[0])).GetCell(33).SetCellValue(item[2]);
                        sheet.GetRow(System.Convert.ToInt32(item[0])).GetCell(34).SetCellValue(item[3]);
                    }
                }
                redact_wb.Write(redact_file);
            }
            prc = new Process();
            prc.StartInfo.FileName = redact_path;
            prc.Start();
            this.Close();
        }

        private void about_program_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This is opensource project\n" +
                "Gitlab: https://gitlab.com/OlegMask/excel-parse.git \n" +
                "Программа написана на языке C# WPF с использованием технологии .Net 4.8", 
                "О программе", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void about_engineer_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Created by mr. Mask\n" +
            "По всем вопросам можно обратиться в vk или telegram\n" +
            "telegram: t.me/I_am_mr_Mask\n" +
            "vk: vk.com/iggggoooorrrr", "О разработчике", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void instruction_Click(object sender, RoutedEventArgs e)
        {
            Instruction ins = new Instruction();
            ins.ShowDialog();
        }
    }
}